import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

df = pd.read_csv('Advertising.csv', sep=',', index_col='id')
sns.pairplot(df)
plt.show()

fig, axs = plt.subplots(1, 3, sharey=True)
df.plot(kind='scatter', x='TV', y='sales', ax=axs[0], figsize=(16, 8))
df.plot(kind='scatter', x='radio', y='sales', color='red', ax=axs[1])
df.plot(kind='scatter', x='newspaper', y='sales', color='green', ax=axs[2])
plt.show()

import statsmodels.formula.api as smf
lm = smf.ols(formula='sales~TV', data=df).fit()
print(lm.params)

X_new = pd.DataFrame({'TV': [50]})
print(X_new.head())
print(lm.predict(X_new))